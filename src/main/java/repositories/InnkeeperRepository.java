/*
 * ActorRepository.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Innkeeper;

@Repository
public interface InnkeeperRepository extends JpaRepository<Innkeeper, Integer> {

	@Query("select c from Innkeeper c where c.userAccount.id = ?1")
	Innkeeper findByUserAccountId(int id);

}
