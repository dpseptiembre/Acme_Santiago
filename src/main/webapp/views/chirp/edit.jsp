<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="chirp/edit.do" modelAttribute="chirp">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="owner" />
	<form:hidden path="hidden" />
	<form:hidden path="creationMomment" />
	
	<acme:textbox path="title" code="chirp.title" />
	<br />
	<acme:textbox path="description" code="chirp.description" />
	<br />
	
		<!---------------------------- BOTONES -------------------------->


	<input type="submit" name="save"
		value="<spring:message code="chirp.save" />" />

    <input type="button" name="cancel"
		value="<spring:message code="chirp.cancel" />"
		onclick="javascript: window.location.replace('chirp/list.do')" />

</form:form>