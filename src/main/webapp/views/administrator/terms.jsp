<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<h2>1. Who we are</h2>
<p align="justify"> Our website address is: <a href="https://acme.com/acme-santiago/">https://acme.com/acme-santiago/</a></p>
<h2>2. What personal data we collect and why we collect it</h2>
<p align="justify"> In the contact form of the main page, we ask for your name and email to be able to offer you a complete feedback about your question.

    On the other hand bur not least, we get your IP and your browsing data meanwhile you are visiting our web with Google Analytics tool. All that collected data is not related with anything that identifies you or your IP.</p>
<h2>3. Which is the target of collecting personal data</h2>
<p align="justify">As we said before, you personal data is used for:</p>
<p align="padding-left: 15px;">- Improve the user experience of our site.
    - To be able of providing fully personalised attention on any of your interactions with me.
    - To avoid spam and scam.
    - To get you opinion.</p>

<h2>4. Where and how long am I going to store your personal data</h2>
<p align="justify">All your personal data will be hosted and secured in a server located in Gravelines (France). That data is encrypted and each transaction with the host server is secured too from end to end. You can check our certificate from any web browser.
    That data will not be removed as long as you ask for it still you use the right of erasing it exposed in the section 6 of this document.</p>
<h2>5. Does third parties receive data from us?</h2>
<p align="justify">Nope, never ever.</p>
<h2>6. What rights you have over your data</h2>
<p align="justify">You can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request the removal of any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes. You can use your right by sending an email to: <a href="mailto:admin@acmesantiago.com?Subject=PrivacyPolicy" target="_top">admin@acmesantiago.com</a> and we'll be delighted to help you.
<h2>7. Why a route or a hike may be considered inappropriate?</h2>
<p align="justify">
An administrator could remove a route that he or she thinks is inappropriate if it contains offensive words on the text.
    Removing a route involves removing all of the hikes of which it is composed.
</p>

Cheers!