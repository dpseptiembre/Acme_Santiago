/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package converters;

import org.springframework.core.convert.converter.Converter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class DateToStringConverter implements Converter<Date, String> {

   @Override
   public String convert(Date date) {
      String result;
      SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

      if (date == null)
         result = null;
      else
//         result = date.toString().replace("-", "/");

         result = myFormat.format(date);

      return result;
   }
}
