<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<%--
  Created by IntelliJ IDEA.
  User: mruwzum
  Date: 19/12/16
  Time: 23:24
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<br>

<div class="container">
<div class="card-deck">
    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q1" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q1}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q2" var="q2b"/>
                <jstl:out value="${q2b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q2}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q3" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q3}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q4" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q4}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

</div>


<hr>
<div class="card-deck">
    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q5" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q5}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q6" var="q2b"/>
                <jstl:out value="${q2b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q6}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q7" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q7}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q8" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q8}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

</div>


<hr>
<div class="card-deck">
    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q9" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q9}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q10" var="q2b"/>
                <jstl:out value="${q2b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q10}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q11" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q11}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q12" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q12}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

</div>


<hr>
<div class="card-deck">
    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q13" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q13}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q14" var="q2b"/>
                <jstl:out value="${q2b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q14}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->


</div>

    <!--/.Panel-->
</div>
    <hr>


