
package services;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Chirp;
import domain.Comment;
import domain.Route;
import domain.Utls;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class AdministratorServiceTest extends AbstractTest {

	@Autowired
	private AdministratorService	administratorService;
	@Autowired
	private RouteService			routeService;
	@Autowired
	private UtlsService				utlsService;


	@Test
	public void removeRoute() {
		this.authenticate("administrator1");
		final List<Route> routes = new ArrayList<>(this.routeService.findAll());
		final Integer initialSize = routes.size();
		routes.remove(0);
		final Integer finalSize = routes.size();
		org.junit.Assert.assertNotEquals(initialSize, finalSize);
		this.unauthenticate();
	}

	@Test
	public void getTabooWords() {
		this.authenticate("administrator1");
		final List<Utls> tabooWords = (List<Utls>) this.utlsService.findAll();
		if (tabooWords.isEmpty())
			throw new NoSuchElementException("The taboo words list is empty");
		else {
			final Utls first = tabooWords.get(0);
			Assert.assertNotNull(first);
		}
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void getTabooWordsNegative() {
		this.authenticate("administrator1");
		final List<Utls> tabooWords = (List<Utls>) this.utlsService.findAll();
		if (tabooWords.isEmpty())
			throw new NoSuchElementException("The taboo words list is empty");
		else {
			final Utls first = tabooWords.get(80);
			Assert.assertNotNull(first);
		}
	}

	@Test
	public void editTabooWord() {
		this.authenticate("administrator1");
		final Utls tabooWord = this.utlsService.create();
		Assert.assertNotNull(tabooWord);
		final Utls tabooWord2 = this.utlsService.save(tabooWord);
		Assert.assertNotNull(tabooWord2);
	}

	@Test
	public void deleteTabooWord() {
		this.authenticate("administrator1");
		final List<Utls> tabooWords = new ArrayList<>(this.utlsService.findAll());
		final Integer initialSize = tabooWords.size();
		tabooWords.remove(0);
		final Integer finalSize = tabooWords.size();
		org.junit.Assert.assertNotEquals(initialSize, finalSize);
		this.unauthenticate();
	}

	@Test
	public void tabooChirps() {
		this.authenticate("administrator1");
		final List<Chirp> chirps = (List<Chirp>) this.administratorService.getChirpWithTabooWords();
		Assert.assertNotNull(chirps);
		this.unauthenticate();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void tabooChirpsNegative() {
		this.authenticate("administrator1");
		final List<Chirp> chirps = (List<Chirp>) this.administratorService.getChirpWithTabooWords();
		Assert.assertNotNull(chirps.get(80));
		this.unauthenticate();
	}

	@Test
	public void removeChirp() {
		this.authenticate("administrator1");
		final List<Chirp> chirps = (List<Chirp>) this.administratorService.getChirpWithTabooWords();
		final Integer initialSize = chirps.size();
		chirps.remove(0);
		final Integer finalSize = chirps.size();
		org.junit.Assert.assertNotEquals(initialSize, finalSize);
		this.unauthenticate();
	}

	@Test
	public void tabooComments() {
		this.authenticate("administrator1");
		final List<Comment> chirps = (List<Comment>) this.administratorService.getCommentsWithTabooWords();
		Assert.assertNotNull(chirps);
		this.unauthenticate();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void tabooCommentsNegative() {
		this.authenticate("administrator1");
		final List<Comment> chirps = (List<Comment>) this.administratorService.getCommentsWithTabooWords();
		Assert.assertNotNull(chirps.get(80));
		this.unauthenticate();
	}

	@Test
	public void removeComments() {
		this.authenticate("administrator1");
		final List<Comment> chirps = (List<Comment>) this.administratorService.getCommentsWithTabooWords();
		final Integer initialSize = chirps.size();
		chirps.remove(0);
		final Integer finalSize = chirps.size();
		org.junit.Assert.assertNotEquals(initialSize, finalSize);
		this.unauthenticate();
	}

}
