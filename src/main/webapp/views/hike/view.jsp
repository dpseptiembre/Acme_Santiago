<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%--
  ~ Copyright  2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<!-- Listing grid -->

<div class="row">
	<div class="col-8">
		<h2><jstl:out value="${h.name}"/></h2>
		<h3><jstl:out value="${h.lgt}"/></h3>
		<jstl:out value="${h.level}"/>

		<hr>
		<c:forEach var = "listValue" items = "${h.pictures}">
				<img src="${listValue}" width="130" height="130" >
		</c:forEach>
		<hr>
		<c:forEach var = "listValue2" items = "${h.comments}">
			<h2><jstl:out value="${listValue2.title}"/></h2>
			<br>
			<jstl:out value="${listValue2.body}"/>
		</c:forEach>

	</div>
	<div class="col-4">
		<img src="${ad}">
	</div>
</div>