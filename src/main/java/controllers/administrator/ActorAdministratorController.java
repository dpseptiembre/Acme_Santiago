/*
 * ActorAdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Collection;
import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import controllers.AbstractController;
import services.AdministratorService;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/administrator")
public class ActorAdministratorController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private ActorService actorService;
	@Autowired
	private AdministratorService administratorService;

	// Constructors -----------------------------------------------------------

	public ActorAdministratorController() {
		super();
	}

	// Listing ----------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Actor> actors;

		actors = this.actorService.findAll();
		result = new ModelAndView("actor/list");
		result.addObject("actors", actors);

		return result;
	}

	// Creation ---------------------------------------------------------------

	// Taboo ----------------------------------------------------------------

	@RequestMapping(value = "/utilsView", method = RequestMethod.GET)
	public ModelAndView utilsView() {

		ModelAndView result;
		Utls uttils1 = administratorService.getUtil();

		Collection<String> strings = uttils1.getTabooWords();
		result = new ModelAndView("administrator/utils");
		result.addObject("strings", strings);
		return result;
	}

	@RequestMapping(value = "/createTaboo", method = RequestMethod.GET)
	public ModelAndView createSpam() {

		ModelAndView result;

		try {
			result = new ModelAndView("administrator/spam");
			return result;
		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
			return result;
		}

	}

	@RequestMapping(value = "/addTaboo", method = RequestMethod.GET)
	public ModelAndView addSpamWord(@RequestParam String word) {

		ModelAndView result;

		try {
			administratorService.addTabooWord(word);
			result = new ModelAndView("administrator/success");
			return result;
		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
			return result;
		}

	}

	@RequestMapping(value = "/deleteTaboo", method = RequestMethod.GET)
	public ModelAndView deleteSpam(@RequestParam String word) {

		ModelAndView result;

		try {
			administratorService.removeTabooWord(word);
			result = new ModelAndView("administrator/success");
			return result;
		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
			return result;
		}

	}

	// Routes ----------------------------------------------------------------

	@RequestMapping(value = "/removeRoute", method = RequestMethod.GET)
	public ModelAndView removeRoute(@RequestParam int routeId) {
		ModelAndView result;

		try {
			administratorService.removeRoute(routeId);
			result = new ModelAndView("administrator/success");

		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	// Chirps ----------------------------------------------------------------

	@RequestMapping(value = "/listTabooChirps", method = RequestMethod.GET)
	public ModelAndView listTabooChirps() {
		ModelAndView result;

		try {
			Collection<Chirp> chirps = administratorService
					.getChirpWithTabooWords();
			result = new ModelAndView("chirp/list");
			result.addObject("chirps", chirps);
		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	@RequestMapping(value = "/removeChirp", method = RequestMethod.GET)
	public ModelAndView removeChirp(@RequestParam int chirpId) {
		ModelAndView result;

		try {
			administratorService.removeChirp(chirpId);
			result = new ModelAndView("administrator/success");

		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	// Comments ----------------------------------------------------------------

	@RequestMapping(value = "/listTabooComments", method = RequestMethod.GET)
	public ModelAndView listTabooComments() {
		ModelAndView result;

		try {
			Collection<Comment> comments = administratorService
					.getCommentsWithTabooWords();
			result = new ModelAndView("comment/list");
			result.addObject("comments", comments);
		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	@RequestMapping(value = "/removeComment", method = RequestMethod.GET)
	public ModelAndView removeComment(@RequestParam int commentId) {
		ModelAndView result;

		try {
			administratorService.removeComment(commentId);
			result = new ModelAndView("administrator/success");

		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}
	
				
	// Ancillary methods ------------------------------------------------------

	@RequestMapping(value = "/terms", method = RequestMethod.GET)
	public ModelAndView terms() {
		ModelAndView result;

		try {
			result = new ModelAndView("administrator/terms");

		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	// Dashboard ------------------------------------------------------

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public ModelAndView dashboard() {

		ModelAndView result;

		try {

			double q1 = administratorService.averageRouterPerUser();
			double q2 = administratorService.standarDesviationOfRoutesPerUser();
			double q3 = administratorService.averageOfHikesPerRoute();
			double q4 = administratorService.standarDesviationOfHikesPerRoute();
			double q5 = administratorService.averageOfRoutesLenght();
			double q6 = administratorService.standarDesviationOfRoutesLenght();
			double q7 = administratorService.averageOfHikesLenght();
			double q8 = administratorService.standarDesviationOfHikesLenght();

			double q9 = administratorService.averageNumberOfChirpsPerUser();
			double q10 = administratorService
					.averageNumberOfCommentsPerRouteIncludingHikes();
			double q11 = administratorService
					.averageNumberOfInnsManagerPerKeepeer();
			double q12 = administratorService
					.standarDesviationOfInnsManagedPerKeeper();

			double q13 = administratorService
					.avgNumberOfuserRegistrationsPerDay();
			double q14 = administratorService
					.standarDesviationOfUserRegistrationPerDay();


			result = new ModelAndView("administrator/dashboard");

			result.addObject("q1", q1);
			result.addObject("q2", q2);
			result.addObject("q3", q3);
			result.addObject("q4", q4);
			result.addObject("q5", q5);
			result.addObject("q6", q6);
			result.addObject("q7", q7);
			result.addObject("q8", q8);
			result.addObject("q9", q9);
			result.addObject("q10", q10);
			result.addObject("q11", q11);
			result.addObject("q12", q12);
			result.addObject("q13", q13);
			result.addObject("q14", q14);



		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;

	}

	@RequestMapping(value = "/downloadPdf", method = RequestMethod.GET)
	public void getPDF1(HttpServletResponse response) {
		response.setContentType("application/pdf");
		response.setHeader("Content-disposition", "attachment;filename="
				+ "compostela.pdf");

		try {
			File f = new File(System.getProperty("user.home")
					+ "/compostela.pdf");
			FileInputStream fis = new FileInputStream(f);
			DataOutputStream os = new DataOutputStream(
					response.getOutputStream());
			response.setHeader("Content-Length", String.valueOf(f.length()));
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = fis.read(buffer)) >= 0) {
				os.write(buffer, 0, len);
			}
			f.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
