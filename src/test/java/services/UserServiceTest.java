
package services;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import security.UserAccount;
import security.UserAccountService;
import utilities.AbstractTest;
import domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class UserServiceTest extends AbstractTest {

	@Autowired
	private UserService			userService;
	@Autowired
	private UserAccountService	userAccountService;


	@Test
	public void registerAsUser() {
		final User u = this.userService.create();
		Assert.notNull(u);
		u.getUserAccount().setUsername("kkkkkk");
		u.setName("kkkkkk");
		u.setSurname("sdfsdf");
		u.setEmail("perri@gjail.com");
		Md5PasswordEncoder encoder;
		encoder = new Md5PasswordEncoder();
		final String hash = encoder.encodePassword("kkkkkk", null);
		u.getUserAccount().setPassword(hash);
		org.junit.Assert.assertNotNull(u);
	}

	@Test(expected = AssertionError.class)
	public void registerAsUserNegative() {
		final User u = this.userService.create();
		u.setName("perrito");
		u.getUserAccount().setUsername("perrito");
		u.getUserAccount().setPassword("perrito");
		Assert.notNull(u);
		u.getUserAccount().setUsername(u.getUserAccount().getUsername());
		Md5PasswordEncoder encoder;
		encoder = new Md5PasswordEncoder();
		final String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
		u.getUserAccount().setPassword(hash);
		final UserAccount userAccount = this.userAccountService.save(u.getUserAccount());
		u.setUserAccount(userAccount);
		Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
		final User resu = this.userService.save(u);
		org.junit.Assert.assertNotNull(resu.getEmail());
	}

	@Test
	public void listUsers() {
		final List<User> users = (List<User>) this.userService.findAll();
		Assert.notNull(users);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void listUsersNegative() {
		final List<User> users = (List<User>) this.userService.findAll();
		Assert.notNull(users.get(80));
	}

	@Test
	public void followUser() {
		this.authenticate("user1");
		final List<User> users = (List<User>) this.userService.findAll();
		final User res = users.get(0);
		final User aux = users.get(1);
		res.getFollowing().add(aux);
		res.setFollowing(res.getFollowing());
		Assert.isTrue(res.getFollowing().contains(aux));
		this.unauthenticate();
	}

	@Test
	public void unfollowUser() {
		this.authenticate("user1");
		final List<User> users = (List<User>) this.userService.findAll();
		final User res = users.get(0);
		final User aux = users.get(1);
		res.getFollowing().add(aux);
		res.setFollowing(res.getFollowing());
		res.getFollowing().remove(aux);
		res.setFollowing(res.getFollowing());
		Assert.isTrue(!res.getFollowing().contains(aux));
		this.unauthenticate();
	}

	@Test
	public void listFollowers() {
		this.authenticate("user1");
		final User u = this.userService.findByPrincipal();
		final Collection<User> users = u.getFollowers();
		Assert.notNull(users);
		this.unauthenticate();
	}

	@Test(expected = ClassCastException.class)
	public void listFollowersNegative() {
		this.authenticate("user1");
		final User u = this.userService.findByPrincipal();
		final List<User> users = (List<User>) u.getFollowers();
		Assert.notNull(users.get(80));
		this.unauthenticate();
	}

	@Test
	public void listFollowing() {
		this.authenticate("user1");
		final User u = this.userService.findByPrincipal();
		final Collection<User> users = u.getFollowing();
		Assert.notNull(users);
		this.unauthenticate();
	}

	@Test(expected = ClassCastException.class)
	public void listFollowingNegative() {
		this.authenticate("user1");
		final User u = this.userService.findByPrincipal();
		final List<User> users = (List<User>) u.getFollowing();
		Assert.notNull(users.get(80));
		this.unauthenticate();
	}

}
