
package services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Route;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class RouteServiceTest extends AbstractTest {

	@Autowired
	private RouteService	routeService;


	@Test
	public void listRoutes() {
		final List<Route> routes = (List<Route>) this.routeService.findAll();
		Assert.notNull(routes);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void listRoutesNegative() {
		final List<Route> routes = (List<Route>) this.routeService.findAll();
		Assert.notNull(routes.get(80));
	}

	@Test
	public void findRoutes() {
		final List<Route> tutorials = (List<Route>) this.routeService.findByKeyword("a");
		Assert.notNull(tutorials);
	}

	@Test
	public void findEmpty() {
		final List<Route> tutorials = (List<Route>) this.routeService.findByKeyword("");
		Assert.notNull(tutorials);
	}

	@Test
	public void findLength() {
		final List<Route> tutorials = (List<Route>) this.routeService.listByLength(10);
		Assert.notNull(tutorials);
	}

	@Test
	public void findMaxHike() {
		final List<Route> tutorials = (List<Route>) this.routeService.listMaximumHikes(4);
		Assert.notNull(tutorials);
	}

	@Test
	public void findMinHike() {
		final List<Route> tutorials = (List<Route>) this.routeService.listMaximumHikes(1);
		Assert.notNull(tutorials);
	}

	@Test
	public void createRoute() {
		this.authenticate("user1");
		final Route route = this.routeService.create();
		route.setDescription("Description");
		route.setName("Ruta 66");
		route.setLgt(30);

		final Route res = this.routeService.save(route);
		Assert.notNull(res);
		this.unauthenticate();
	}

	@Test(expected = IllegalArgumentException.class)
	public void createRouteNegative() {
		this.authenticate("user1");
		final Route route = this.routeService.create();
		route.setDescription("Description");
		route.setName("Ruta 66");
		route.setLgt(30);

		final Route res = this.routeService.save(route);
		Assert.notNull(res.getComments());
		this.unauthenticate();
	}

	@Test
	public void editRoute() {
		this.authenticate("user1");
		final Route a = this.routeService.create();
		final Route res = this.routeService.save(a);
		Assert.notNull(res);
		this.unauthenticate();
	}

	@Test
	public void removeRoute() {
		this.authenticate("user1");
		final List<Route> routes = new ArrayList<>(this.routeService.findAll());
		final Integer initialSize = routes.size();
		routes.remove(0);
		final Integer finalSize = routes.size();
		org.junit.Assert.assertNotEquals(initialSize, finalSize);
		this.unauthenticate();
	}

}
