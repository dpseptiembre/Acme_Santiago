<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="comments" requestURI="${requestURI}" id="row">

	<security:authorize access="hasRole('ADMINISTRATOR')">
		<display:column>
			<a class="badge badge-danger" href="administrator/removeComment.do?commentId=${row.id}" onclick="return confirm('<spring:message code="general.confirm"/>') "><spring:message code="general.delete"/></a>
		</display:column>
	</security:authorize>

	<spring:message code="chirp.title" var="title" />
	<display:column property="title" title="${title}" sortable="false" />
	<spring:message code="comment.stars" var="stars" />
	<display:column property="stars" title="${stars}" sortable="false" />
	<spring:message code="comment.body" var="body" />
	<display:column property="body" title="${body}" sortable="false" />
    <c:forEach var = "listValue" items = "${row.pictures}">
        <spring:message code="amenities.pictures" var="pictures" />
        <display:column title="${pictures}">
            <img src="${listValue}" width="130" height="130" >
        </display:column>
    </c:forEach>
</display:table>	