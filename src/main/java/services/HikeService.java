/*
 * HikeService.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;
import domain.Comment;
import domain.Hike;
import domain.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.HikeRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Service
@Transactional
public class HikeService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private HikeRepository		hikeRepository;
	@Autowired
	private RouteService routeService;

	// Supporting services ----------------------------------------------------



	// Constructors -----------------------------------------------------------

	public HikeService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	public Hike create(int routeId){
		Hike result;
		result = new Hike();
		Route route = routeService.findOne(routeId);
		result.setHidden(false);
		result.setComments(new ArrayList<Comment>());
		result.setRoute(route);
		return result;
	}
	
	public Collection<Hike> findAll() {
		Collection<Hike> result;

		result = hikeRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Hike findOne(int hikeId) {
		Assert.isTrue(hikeId != 0);

		Hike result;

		result = hikeRepository.findOne(hikeId);
		Assert.notNull(result);

		return result;
	}

	public Hike save(Hike hike) {
		Assert.notNull(hike);

		Hike result;

		result = hikeRepository.save(hike);

		return result;
	}

	public void delete(Hike hike) {
		Assert.notNull(hike);
		Assert.isTrue(hike.getId() != 0);
		Assert.isTrue(hikeRepository.exists(hike.getId()));

		hikeRepository.delete(hike);
	}

	public boolean checkDay(Hike hike) {

		Route route = hike.getRoute();
		Boolean res = true;
		List<Hike> hikes = new ArrayList<>(route.getHikes());
		for (Hike h: hikes){
			if(h.getDay()==hike.getDay()){
				res = false;
				break;
			}
		}
		return res;
	}

	// Other business methods -------------------------------------------------


}
