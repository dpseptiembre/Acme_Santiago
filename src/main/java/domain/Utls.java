/*
 * Administrator.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Utls extends DomainEntity {

	// Constructors -----------------------------------------------------------

	public Utls() {
		super();
	}

	// Attributes -------------------------------------------------------------

	private Collection<String> tabooWords;

	// Relationships ----------------------------------------------------------


	@ElementCollection
	public Collection<String> getTabooWords() {
		return tabooWords;
	}

	public void setTabooWords(Collection<String> tabooWords) {
		this.tabooWords = tabooWords;
	}
}
