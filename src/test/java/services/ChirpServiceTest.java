
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Chirp;
import domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class ChirpServiceTest extends AbstractTest {

	@Autowired
	private ChirpService	chirpService;
	@Autowired
	private UserService		userService;


	@Test
	public void createChirp() {
		this.authenticate("user1");
		final Chirp chirp = this.chirpService.create();
		chirp.setDescription("Desc");
		chirp.setTitle("Titulo");

		final Chirp res = this.chirpService.save(chirp);
		Assert.notNull(res);
		this.unauthenticate();
	}

	@Test(expected = IllegalArgumentException.class)
	public void createChirpNegative() {
		this.authenticate("user1");
		final Chirp chirp = this.chirpService.create();
		chirp.setDescription("Desc");
		chirp.setTitle("Titulo");

		final Chirp res = this.chirpService.save(chirp);
		Assert.notNull(res.getOwner());
		this.unauthenticate();
	}

	@Test
	public void followingChirp() {
		this.authenticate("user1");
		final User user = this.userService.findByPrincipal();
		final Collection<User> users = user.getFollowing();
		final List<Chirp> chirps = new ArrayList<Chirp>();
		for (final User u : users)
			chirps.addAll(u.getChirps());
		Assert.notNull(chirps);
		this.unauthenticate();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void followingChirpNegative() {
		this.authenticate("user1");
		final User user = this.userService.findByPrincipal();
		final Collection<User> users = user.getFollowing();
		final List<Chirp> chirps = new ArrayList<Chirp>();
		for (final User u : users)
			chirps.addAll(u.getChirps());
		Assert.notNull(chirps.get(80));
		this.unauthenticate();
	}

}
