/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StringToDateConverter implements Converter<String, Date> {


   @SuppressWarnings("deprecation")
   @Override
   public Date convert(String source) {
      Date result;
      SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
      try {
         if (StringUtils.isEmpty(source))
            result = null;
         else {
            result = myFormat.parse(source);
//            result = new Date(source);
         }
      } catch (Throwable oops) {
         throw new IllegalArgumentException(oops);
      }
      return result;
   }
}
