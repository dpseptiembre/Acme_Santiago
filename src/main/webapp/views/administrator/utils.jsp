<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<spring:message code="utils.spam" var="spam"/>


<div class="container">

    <h2><jstl:out value="${spam}"/></h2>
    <a href="administrator/createTaboo.do" class="btn btn-primary"><spring:message code="general.create" /></a>

    <table class="table table-striped">
            <c:forEach var = "listValue" items = "${strings}">
                <tr>
                <td>
                    <c:out value="${listValue}"/>
                </td>
                    <td>
                       <a href="administrator/deleteTaboo.do?word=${listValue}" onclick="return confirm('<spring:message code="general.confirm"/>')" class="btn btn-danger"><spring:message code="general.delete" /></a>
                    </td>
                </tr>
            </c:forEach>

    </table>


    <%--<h2><spring:message code="admin.searchResults"/></h2>--%>

    <%--<form:form action="administrator/resultNumber.do" method="get">--%>
        <%--<input placeholder="${searchN}" name="num" class="form-group" type="number"/>--%>
        <%--<input type="submit" value="<spring:message code="admin.changeResults" />" class="btn btn-primary"/>--%>
    <%--</form:form>--%>


    <%--<h2><spring:message code="admin.finderCache"/></h2>--%>
    <%--<form:form action="administrator/setFinder.do" method="get">--%>
        <%--<input placeholder="${cache}" name="cache" class="form-group" type="number"/>--%>
        <%--<input type="submit" value="<spring:message code="admin.changeResults" />" class="btn btn-primary"/>--%>
    <%--</form:form>--%>

    <%--<h2><spring:message code="admin.sysemCustom"/></h2>--%>
    <%--<form:form action="administrator/customize.do" method="get">--%>
        <%--<p><spring:message code="admin.systemName"/></p>--%>
        <%--<input value="${systemName}" name="systemName" class="form-group" type="text" title="SystemName"/>--%>
        <%--<br>--%>
        <%--<p><spring:message code="admin.banner"/></p>--%>
        <%--<input value="${banner}" name="banner" class="form-group" type="text"/>--%>
        <%--<br>--%>
        <%--<p><spring:message code="admin.welcome"/></p>--%>
        <%--<input value="${welcome}" name="welcome" class="form-group" type="text"/>--%>
        <%--<br>--%>
        <%--<p><spring:message code="admin.welcomeES"/></p>--%>
        <%--<input value="${welcomeES}" name="welcomeES" class="form-group" type="text"/>--%>
        <%--<br>--%>
        <%--<input type="submit" value="<spring:message code="admin.changeResults" />" class="btn btn-primary"/>--%>
    <%--</form:form>--%>

</div>