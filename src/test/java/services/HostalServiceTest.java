
package services;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Hostal;
import domain.Innkeeper;
import domain.Registration;
import domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class HostalServiceTest extends AbstractTest {

	@Autowired
	private HostalService		hostalService;
	@Autowired
	private InnkeeperService	innkeeperService;
	@Autowired
	private UserService			userService;
	@Autowired
	private RegistrationService	registrationService;


	@Test
	public void listHostals() {
		final List<Hostal> routes = (List<Hostal>) this.hostalService.findAll();
		Assert.notNull(routes);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void listHostalsNegative() {
		final List<Hostal> routes = (List<Hostal>) this.hostalService.findAll();
		Assert.notNull(routes.get(80));
	}

	@Test
	public void registerUser() {
		this.authenticate("innkeeper1");
		final Innkeeper i = this.innkeeperService.findByPrincipal();
		final List<Hostal> h = (List<Hostal>) i.getHostals();
		final List<User> users = (List<User>) this.userService.findAll();
		final Registration r = this.registrationService.create();
		r.setStartDate(new Date(System.currentTimeMillis() - 1000));
		r.setHosted(users.get(0));
		r.setHostal(h.get(0));
		Assert.notNull(r);
		this.unauthenticate();
	}

	@Test(expected = IllegalArgumentException.class)
	public void registerUserNegative() {
		this.authenticate("innkeeper1");
		final Innkeeper i = this.innkeeperService.findByPrincipal();
		final List<Hostal> h = (List<Hostal>) i.getHostals();
		final List<User> users = (List<User>) this.userService.findAll();
		final Registration r = this.registrationService.create();
		r.setStartDate(new Date(System.currentTimeMillis() - 1000));
		r.setHosted(users.get(0));
		r.setHostal(h.get(0));
		Assert.notNull(r.getEndDate());
		this.unauthenticate();
	}

}
