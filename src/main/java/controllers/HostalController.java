
package controllers;

import java.util.Collection;

import domain.Hostal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.HostalService;

@Controller
@RequestMapping("/hostal")
public class HostalController extends AbstractController {

	public HostalController() {
		super();
	}


	@Autowired
	private HostalService hostalService;

	protected static ModelAndView createEditModelAndView(final Hostal hostal) {
		ModelAndView result;

		result = HostalController.createEditModelAndView(hostal, null);

		return result;
	}

	protected static ModelAndView createEditModelAndView(final Hostal hostal, final String message) {
		ModelAndView result;

		result = new ModelAndView("hostal/edit");
		result.addObject("hostal", hostal);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView hostalsList() {
		ModelAndView result;
		final Collection<Hostal> hostals;
		hostals=hostalService.findAll();
		result = new ModelAndView("hostal/list");
		result.addObject("hostals", hostals);
		result.addObject("requestURI", "hostal/list.do");
		return result;
	}
	
	@RequestMapping(value = "/validlist", method = RequestMethod.GET)
	public ModelAndView hostalsValidList() {
		ModelAndView result;
		final Collection<Hostal> hostals;
		hostals = this.hostalService.findAllValid();
		result = new ModelAndView("hostal/list");
		result.addObject("hostals", hostals);
		result.addObject("requestURI", "hostal/list.do");
		return result;
	}


	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam int hostalId) {
		ModelAndView result;
		try {
			Hostal hostal;
			hostal = hostalService.findOne(hostalId);
			Assert.notNull(hostal);
			result = new ModelAndView("hostal/view");
			result.addObject("in", hostal);
		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
			return result;
		}

		return result;
	}

}
