/*
 * Administrator.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

@Entity
@Access(AccessType.PROPERTY)
public class Chirp extends DomainEntity {

	// Constructors -----------------------------------------------------------

	public Chirp() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private Date	creationMomment;
	private String	title, description;
	private User	owner;
	private Boolean	hidden;


	// Relationships ----------------------------------------------------------

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreationMomment() {
		return this.creationMomment;
	}

	public void setCreationMomment(final Date creationMomment) {
		this.creationMomment = creationMomment;
	}

	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@ManyToOne
	public User getOwner() {
		return this.owner;
	}

	public void setOwner(final User owner) {
		this.owner = owner;
	}

	public Boolean getHidden() {
		return this.hidden;
	}

	public void setHidden(final Boolean hidden) {
		this.hidden = hidden;
	}
}
