<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="route/edit.do" modelAttribute="route">
	
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="owner" />
	<form:hidden path="hikes" />
	<form:hidden path="hidden" />
	<form:hidden path="comments" />
	
	<acme:textbox path="name" code="route.name" />
	<br />
	<acme:textbox path="description" code="route.description" />
	<br />
	<acme:textbox path="lgt" code="route.lenght" />
	<br />
	<acme:textbox path="pictures" code="route.pictures" />
	<br />
	
	<input type="submit" name="save"
		value="<spring:message code="route.save" />" />&nbsp; 
	<jstl:if test="${route.id != 0}">
	<input type="submit" name="delete"
			value="<spring:message code="route.delete" />"
			onclick="return confirm('<spring:message code="route.confirm.delete" />')" />&nbsp;
	</jstl:if>
	<input type="button" name="cancel"
		value="<spring:message code="route.cancel" />"
		onclick="javascript: relativeRedir('route/myList.do');" />
	<br />

</form:form>